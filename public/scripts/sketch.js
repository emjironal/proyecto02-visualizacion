var song, fft, type
const WAVES = 0, BARS = 1, CIRCLEBARS = 2, CIRCLES = 3, FULLSCREEN = 4, CIRCLES2 = 5
var btnVisualizationWaves, btnVisualizationBars, btnVisualizationCircleWithBars, btnVisualizationCircles,
    btnPlay, btnType
var canvas
const usedScreen = 1.11

function preload()
{
    song = loadSound("../assets/Playtime.mp3")
}

function newFtt()
{
    //Music
    fft = new p5.FFT()
    fft.setInput(song)
}

function setup()
{
    //Default visualization
    type = CIRCLES
    btnType = document.getElementById("circles")

    //Canvas
    canvas = createCanvas(windowWidth / usedScreen, windowHeight / usedScreen)
    canvas.parent("canvas-container")
    canvas.class("col-md-auto")
    
    newFtt()
    
    loadingSong(false)
}

function changeType(newType, caller)
{
    type = newType
    btnType.classList.remove("active")
    btnType = document.getElementById(caller)
    btnType.classList.add("active")
}

function draw()
{
    background(0, 0, 0)

    let spectrum = fft.analyze()

    noStroke()
    switch(type)
    {
        case WAVES:
            visualizationWaves(spectrum)
            break
        case BARS:
            visualizationBars(spectrum)
            break
        case CIRCLEBARS:
            visualizationCircleWithBars(spectrum)
            break
        case FULLSCREEN:
            visualizationFullScreen(spectrum)
            break
        case CIRCLES2:
            visualizationCirclesUpgraded(spectrum)
            break
        default:
            visualizationCircles(spectrum)
            break
    }
    fill("#A9A9F5")
    progress = map(song.currentTime(), 0, song.duration(), 0, width)
    rect(0, 0, progress, 4);
}

//Extracted from: https://p5js.org/reference/#/p5/resizeCanvas
function windowResized()
{
    resizeCanvas(windowWidth / usedScreen, windowHeight / usedScreen)
}

function btnPauseListener()
{
    btn = document.getElementById("btnPlay")
    if (song.isPlaying())
    {
        song.pause()
        btn.innerHTML = '<i class="fas fa-play"></i>'
    }
    else
    {
        song.play()
        btn.innerHTML = '<i class="fas fa-pause"></i>'
    }
}

function changeAudioFile()
{
    //Change label text
    var lbInputAudioFile = document.getElementById("lbInputAudioFile")
    lbInputAudioFile.textContent = document.getElementById("inputAudioFile").files[0].name

    var reader = new FileReader();
    reader.onload = function (element)
    {
        song.stop()
        song.disconnect()
        song = loadSound(element.target.result,
            ()=>
            {
                loadingSong(false)
            },
            (error)=>{print(error)}
            ,
            (progress)=>
            {
                loadingSong(true)
            }
        )
        newFtt()
    }
    reader.readAsDataURL(document.getElementById("inputAudioFile").files[0]);
}

function loadingSong(isLoading)
{
    btn = document.getElementById("btnPlay")
    loading = document.getElementById("loading")

    btn.hidden = isLoading
    loading.hidden = !isLoading

    btn.innerHTML = '<i class="fas fa-play"></i>'
}