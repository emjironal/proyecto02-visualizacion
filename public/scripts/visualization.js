const usedData = 10

function visualizationWaves(spectrum)
{
    fill(0, 0, 255)
    beginShape()
    vertex(0, height)
    datalenght = spectrum.length / 2
    for (i = 0; i < datalenght; i++)
    {
        vertex(map(i, 0, datalenght - 1, 0, width), map(spectrum[i], 0, 255, height, 0))
    }
    vertex(width, height)
    endShape()
}

function visualizationBars(spectrum)
{
    //Extracted from: https://editor.p5js.org/p5/sketches/Sound:_Note_Envelope
    datalenght = spectrum.length / usedData
    for (let i = 0; i < datalenght; i++)
    {
        fill(0, spectrum[i] / 10, spectrum[i])
        let x = map(i, 0, datalenght - 1, 0, width - (width / (datalenght + 1)))
        let h = map(spectrum[i], 0, 255, 0, height - 4)
        rect(x, height, width / (datalenght + 1), -h)
    }
}

function visualizationCircleWithBars(spectrum)
{
    datalenght = spectrum.length / usedData
    begin = 0
    for (let i = 0; i < datalenght; i++)
    {
        fill(0, spectrum[i] / 10, spectrum[i])
        radius = map(spectrum[i], 0, 255, width / 4, width / 2.3)
        finish = ((2 * PI) / (datalenght + 0.5))
        arc(width / 2, height / 2, radius, radius, begin, begin + finish, PIE)
        begin += finish
    }
    fill(0, 0, 0)
    circle(width / 2, height / 2, width / 4)
}

function visualizationCircles(spectrum)
{
    datalenght = spectrum.length / 30
    for (let i = 0; i < datalenght; i++)
    {
        fill(0, spectrum[i] / 10, spectrum[i])
        radius = map(spectrum[i], 0, 255, width / 6, width / 2.3)
        circle(width / 2, height / 2, radius)
    }
    fill(0, 0, 0)
    circle(width / 2, height / 2, width / 6)
}

function visualizationFullScreen(spectrum)
{
    datalenght = spectrum.length / usedData
    xstart = 0
    for (let i = 0; i < datalenght; i++)
    {
        fill(0, 0, spectrum[i])
        rect(xstart, 4, width / datalenght, height - 4)
        xstart += width / datalenght
    }
}

function visualizationCirclesUpgraded(spectrum)
{
    datalenght = spectrum.length / 20
    for (let i = 0; i < datalenght; i++)
    {
        radius = 0
        x = y = 0
        if(i < datalenght / 3)
        {
            x = width / 2
            y = height / 2
            radius = map(spectrum[i], 0, 255, width / 8, width / 3.5)
            fill(0, 0, spectrum[i])
            circle(x, y, radius)
            fill(0, 0, 0)
            circle(width / 2, height / 2, width / 8)
        }
        else
        {
            x = width / 5
            x2 = 4 * x
            if(i < 2 * datalenght / 3)
            {
                y = 3 * height / 4
            }
            else
            {
                y = height / 4
            }
            radius = map(spectrum[i], 0, 255, width / 10, width / 4.3)
            //First half
            fill(0, 0, spectrum[i])
            circle(x, y, radius)
            fill(0, 0, 0)
            circle(x, y, width / 10)
            //Second half
            fill(0, 0, spectrum[i])
            circle(x2, y, radius)
            fill(0, 0, 0)
            circle(x2, y, width / 10)
        }
    }
}